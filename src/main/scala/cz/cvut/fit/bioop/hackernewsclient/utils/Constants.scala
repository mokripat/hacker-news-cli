package cz.cvut.fit.bioop.hackernewsclient.utils

object Constants {
  val DEFAULT_PAGE_SIZE = 5
  val MINIMUM_PAGE_SIZE = 1
  val MAXIMUM_PAGE_SIZE = 100
  val DEFAULT_TTL = 600
}
