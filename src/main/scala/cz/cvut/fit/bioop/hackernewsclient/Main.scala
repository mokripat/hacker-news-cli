package cz.cvut.fit.bioop.hackernewsclient

import cz.cvut.fit.bioop.hackernewsclient.api.item.{HackerNewsCommentsApiImpl, HackerNewsItemApi, HackerNewsStoriesApiImpl}
import cz.cvut.fit.bioop.hackernewsclient.api.user.HackerNewsUsersApiImpl
import cz.cvut.fit.bioop.hackernewsclient.controller.ConsoleController
import cz.cvut.fit.bioop.hackernewsclient.database.InMemoryDatabase
import cz.cvut.fit.bioop.hackernewsclient.database.proxy.DatabaseCacheProxyImpl
import cz.cvut.fit.bioop.hackernewsclient.model.item.{Comment, Story}
import cz.cvut.fit.bioop.hackernewsclient.repository.{CommentsRepository, CommentsRepositoryImpl, StoriesRepositoryImpl, UserRepositoryImpl}
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.FormatExtension.HtlmUnescape
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter
import cz.cvut.fit.bioop.hackernewsclient.utils.parser.command.CommandParser
import cz.cvut.fit.bioop.hackernewsclient.view.ConsoleView

object Main {
  def main(args: Array[String]): Unit = {
    val itemsApi = new HackerNewsItemApi
    val commentsApi = new HackerNewsCommentsApiImpl(itemsApi)
    val storiesApi = new HackerNewsStoriesApiImpl(itemsApi)
    val usersApi = new HackerNewsUsersApiImpl

    val storiesDatabase = new InMemoryDatabase[Long,Story]
    val storiesDatabaseCacheProxy = new DatabaseCacheProxyImpl(storiesDatabase)
    val commentsDatabase = new InMemoryDatabase[Long,Comment]
    val commentsDatabaseCacheProxy = new DatabaseCacheProxyImpl(commentsDatabase)

    val jsonFormatter = new JsonFormatter
    val commandParser = new CommandParser

    val storiesRepository =
      new StoriesRepositoryImpl(storiesApi,storiesDatabaseCacheProxy,jsonFormatter)
    val usersRepository = new UserRepositoryImpl(usersApi, jsonFormatter)
    val commentsRepository = new CommentsRepositoryImpl(commentsApi,commentsDatabaseCacheProxy,jsonFormatter)

    val controller = new ConsoleController(storiesRepository,commentsRepository,usersRepository)
    val consoleView = new ConsoleView(controller,commandParser)

    consoleView.run()
  }
}
