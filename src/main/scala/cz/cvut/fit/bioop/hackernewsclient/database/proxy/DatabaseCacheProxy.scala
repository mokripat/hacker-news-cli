package cz.cvut.fit.bioop.hackernewsclient.database.proxy

import cz.cvut.fit.bioop.hackernewsclient.database.Database
import cz.cvut.fit.bioop.hackernewsclient.utils.Constants.DEFAULT_TTL

/**
 * Proxy for database to check if cached item is still valid (ttl has not run out).
 * @tparam U type of identifier of the item.
 * @tparam V type of the item.
 */
trait DatabaseCacheProxy[U,V] {
  /**
   * Checks if item can be found in database and its validity, if found and not valid, removes it from database.
   * @param itemId ID of wanted item.
   * @return Some[V] if found and valid, None otherwise.
   */
  def get(itemId: U): Option[V]

  /**
   * Persists item if not found or invalid and sets up its validity.
   * @param itemId ID of wanted item to persists.
   * @param item item data.
   * @return true if item was persisted, false otherwise.
   */
  def save(itemId: U, item: V): Boolean

  /**
   * If possible updates item in database and renews its validity.
   * @param itemId ID of wanted item to persists.
   * @param item item data.
   * @return true if item was successfully persisted, false otherwise.
   */
  def update(itemId: U, item: V): Boolean

  /**
   * Sets items TTL in seconds by time when was added or updated.
   * @param ttl time-to-live in seconds
   */
  def setItemsTTL(ttl: Int): Unit

  /**
   * Clears cache metadata and data in database.
   */
  def clear(): Unit
}

class DatabaseCacheProxyImpl[U,V](private val database: Database[U,V]) extends DatabaseCacheProxy[U,V] {
  private var ttl: Int = DEFAULT_TTL
  private var cacheMetadata: Map[U, Long] = Map()

  override def get(itemId: U): Option[V] = {
    database.get(itemId) match {
      case Some(itemDatabase) =>
        cacheMetadata.get(itemId) match {
          case Some(itemAddTime) =>
            if(isItemValid(itemAddTime)) {
              Some(itemDatabase)
            } else {
              database.remove(itemId)
              cacheMetadata -= itemId
              None
            }

          case None =>
            cacheMetadata += (itemId -> currentTimeSeconds())
            Some(itemDatabase)
        }

      case None => None
    }
  }

  override def save(itemId: U, item: V): Boolean =
    cacheMetadata.get(itemId) match {
      case Some(value) => if(!isItemValid(value)) {
        cacheMetadata += (itemId -> currentTimeSeconds())
        database.update(itemId,item)
      } else {
        false
      }
      case None =>
        cacheMetadata += (itemId -> currentTimeSeconds())
        database.save(itemId,item)
    }

  override def update(itemId: U, item: V): Boolean =
    if(database.update(itemId, item)) {
      cacheMetadata += (itemId -> currentTimeSeconds())
      true
    } else
      false


  override def setItemsTTL(ttl: Int): Unit = {
    this.ttl = ttl
  }

  override def clear(): Unit = {
    cacheMetadata = Map()
    database.clear()
  }

  private def isItemValid(itemAddTime: Long): Boolean = {
    itemAddTime + ttl > currentTimeSeconds()
  }

  private def currentTimeSeconds(): Long = System.currentTimeMillis() / 1000
}
