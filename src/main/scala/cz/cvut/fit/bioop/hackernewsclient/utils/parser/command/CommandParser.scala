package cz.cvut.fit.bioop.hackernewsclient.utils.parser.command

import cz.cvut.fit.bioop.hackernewsclient.model.command.Command
import cz.cvut.fit.bioop.hackernewsclient.utils.Constants.{MAXIMUM_PAGE_SIZE, MINIMUM_PAGE_SIZE}
import cz.cvut.fit.bioop.hackernewsclient.utils.parser.StringParser
import cz.cvut.fit.bioop.hackernewsclient.utils.parser.command.CommandParser.{COMMENTS_WRONG_ARGS, NEW_STORIES_WRONG_ARGS, SET_PAGE_SIZE_WRONG_ARGS, TOP_STORIES_WRONG_ARGS, USER_INCOMPLETE}

import java.util.{NoSuchElementException, Scanner}


class CommandParser extends StringParser[Command] {

  override def parse(input: String): Command = {
    val scanner = new Scanner(input)

    val keyCommand = scanner.next()
    keyCommand match {
      case "top-stories" =>
        if (!scanner.hasNext()) {
          Command.TopStories()
        } else {
          try {
            val page = handleArgs(scanner)
            Command.TopStories(page)
          } catch {
            case _: IllegalArgumentException | _: NoSuchElementException => Command.WrongArgs(TOP_STORIES_WRONG_ARGS)
          }
        }
      case "new-stories" =>
        if (!scanner.hasNext()) {
          Command.NewStories()
        } else {
          try {
            val page = handleArgs(scanner)
            Command.NewStories(page)
          } catch {
            case _: IllegalArgumentException | _: NoSuchElementException => Command.WrongArgs(NEW_STORIES_WRONG_ARGS)
          }
        }
      case "comments" =>
        try {
          val storyId = scanner.nextLong()
          if(!scanner.hasNext()) {
            Command.Comments(storyId)
          } else {
            val page = handleArgs(scanner)
            Command.Comments(storyId,page)
          }
        } catch {
          case _: IllegalArgumentException | _: NoSuchElementException => Command.WrongArgs(COMMENTS_WRONG_ARGS)
        }
      case "user" =>
        try {
          val id = scanner.next()
          Command.User(id)
        } catch {
          case _: NoSuchElementException => Command.WrongArgs(USER_INCOMPLETE)
        }
      case "set-page-size" =>
        try {
          val pageSize = scanner.nextInt()
          if(pageSize < MINIMUM_PAGE_SIZE || pageSize > MAXIMUM_PAGE_SIZE)
            throw new IllegalArgumentException()
          Command.SetPageSize(pageSize)
        } catch {
          case _: IllegalArgumentException | _: NoSuchElementException => Command.WrongArgs(SET_PAGE_SIZE_WRONG_ARGS)
        }
      case "clear-cache" => Command.ClearCache
      case "help" => Command.Help
      case "stop" | "exit" => Command.Exit
      case _ => Command.Unknown
    }
  }

  private def handleArgs(scanner: Scanner): Int = {
    var page = 1
    while (scanner.hasNext) {
      val nextArg = scanner.next()
        nextArg match {
          case "-page" => page = scanner.nextInt()
          case _ => throw new IllegalArgumentException()
        }
        if (page < 1) throw new IllegalArgumentException()
      }

    page
  }
}

object CommandParser {
  val USER_INCOMPLETE = "UserId has not been found. Usage: user [userId:String]"
  val SET_PAGE_SIZE_WRONG_ARGS = "Wrong args. Usage: set-page-size [1-100]"
  val TOP_STORIES_WRONG_ARGS = "Wrong args. Usage: top-stories [optional] | optionals: -page [pageNum:Int]"
  val NEW_STORIES_WRONG_ARGS = "Wrong args. Usage: new-stories [optional] | optionals: -page [pageNum:Int]"
  val COMMENTS_WRONG_ARGS = "Wrong args. Usage: comments [storyId:Long] [optional] | optionals: -page [pageNum:Int] "
}
