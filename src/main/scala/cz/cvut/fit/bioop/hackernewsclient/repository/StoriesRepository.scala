package cz.cvut.fit.bioop.hackernewsclient.repository

import cz.cvut.fit.bioop.hackernewsclient.api.item.HackerNewsStoriesApi
import cz.cvut.fit.bioop.hackernewsclient.database.proxy.DatabaseCacheProxy
import cz.cvut.fit.bioop.hackernewsclient.model.item.Story
import cz.cvut.fit.bioop.hackernewsclient.utils.Constants.DEFAULT_PAGE_SIZE
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter
import cz.cvut.fit.bioop.hackernewsclient.utils.repository.PageGetter.getPageFromItems

import scala.collection.mutable.ListBuffer

/**
 * Repository for HackerNews stories.
 */
trait StoriesRepository {

  /**
   * Fetches, formats and returns single story by given ID.
   * @param storyId ID of wanted story.
   * @return Some(Story) if it was successfully fetched and formatted, otherwise None.
   */
  def getStory(storyId: Long) : Option[Story]

  /**
   * Fetches, formats and returns list of top stories.
   * @param count of top stories to get.
   * @return list of current top stories of given count.
   */
  def getTopStories(count: Int = 10) : List[Story]

  /**
   * Fetches, formats and returns [pageIndex]-th page with [pageSize] size of top stories.
   * @param pageIndex index of wanted page.
   * @param pageSize size of one page.
   * @return [pageIndex]-th page with [pageSize] size of top stories.
   */
  def getTopStoriesPage(pageIndex: Int, pageSize: Int = DEFAULT_PAGE_SIZE) : List[Story]

  /**
   * Fetches, formats and returns list of new stories.
   * @param count of top stories to get.
   * @return list of current new stories of given count.
   */
  def getNewStories(count: Int = 10): List[Story]

  /**
   * Fetches, formats and returns [pageIndex]-th page with [pageSize] size of new stories.
   * @param pageIndex index of wanted page.
   * @param pageSize size of one page.
   * @return [pageIndex]-th page with [pageSize] size of new stories.
   */
  def getNewStoriesPage(pageIndex: Int, pageSize: Int = DEFAULT_PAGE_SIZE) : List[Story]

  /**
   * Erases all current persisted data.
   */
  def clearCache(): Unit
}

/**
 * Implementation of StoriesRepository.
 *
 * @param storiesApi Api for fetching stories in raw form (json).
 * @param storyDatabaseProxy Proxy connected to database to persists and handle stories caches.
 * @param jsonFormatter Formatter class to transform raw data to HackerNewsItems.
 */
class StoriesRepositoryImpl (
                              storiesApi: HackerNewsStoriesApi,
                              storyDatabaseProxy: DatabaseCacheProxy[Long,Story],
                              jsonFormatter: JsonFormatter) extends StoriesRepository {

  override def getStory(storyId: Long): Option[Story] =
    storyDatabaseProxy.get(storyId) match {
      case Some(value) => Some(value)
      case None =>
        val rawStory = storiesApi.getStory(storyId)
        val formattedStory = jsonFormatter.itemJsonToStory(rawStory)

        formattedStory match {
          case Some(story) =>
            storyDatabaseProxy.save(story.itemId,story)
            Some(story)
          case None => None
        }
    }

  override def getTopStories(count: Int = 10): List[Story] = {
    val topStoriesIdsRawBox = storiesApi.getTopStoriesIdsBox
    getStoriesFromIds(formatBoxToList(topStoriesIdsRawBox).take(count))
  }

  override def getTopStoriesPage(pageIndex: Int, pageSize: Int = DEFAULT_PAGE_SIZE) : List[Story] = {
    val topStoriesIdsRawBox = storiesApi.getTopStoriesIdsBox
    getStoriesFromIds(
      getPageFromItems(
        formatBoxToList(topStoriesIdsRawBox),
        pageIndex,
        pageSize
      ))
  }

  override def getNewStories(count: Int = 10): List[Story] = {
    val newStoriesIdsRawBox = storiesApi.getNewStoriesIdsBox
    getStoriesFromIds(formatBoxToList(newStoriesIdsRawBox).take(count))
  }

  override def getNewStoriesPage(pageIndex: Int, pageSize: Int = DEFAULT_PAGE_SIZE) : List[Story] = {
    val newStoriesIdsRawBox = storiesApi.getNewStoriesIdsBox
    getStoriesFromIds(
      getPageFromItems(
        formatBoxToList(newStoriesIdsRawBox),
        pageIndex,
        pageSize
    ))
  }

  override def clearCache(): Unit = {
    storyDatabaseProxy.clear()
  }

  /**
   * Tries to get all stories by given IDs, if found in database then loads, else fetches
   * @param storiesIds list containing stories IDs.
   * @return list of stories with given ids.
   */
  private def getStoriesFromIds(storiesIds: List[Long]): List[Story] = {
    val storiesBuffer = new ListBuffer[Story]

    storiesIds
      .foreach(id => {
        getStory(id) match {
          case Some(value) => storiesBuffer.addOne(value)
          case None =>
        }
      })

    storiesBuffer.toList
  }

  private def formatBoxToList(rawBox: String) : List[Long] =
    jsonFormatter.jsonBoxToList(rawBox) match {
      case Some(value) => value
      case None => List()
    }
}
