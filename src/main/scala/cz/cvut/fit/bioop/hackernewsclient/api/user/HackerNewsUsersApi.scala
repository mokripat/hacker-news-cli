package cz.cvut.fit.bioop.hackernewsclient.api.user

import java.net.URL
import scala.io.Source

//testAccount mokripatTest:mokripatTest

/**
 * Serves for fetching raw users data (json) from HackerNews API.
 */
trait HackerNewsUsersApi {
  /**
   * Fetch a user in json by given user id.
   *
   * @param userId id of the user.
   * @return
   */
  def getUserJson(userId: String): String
}

class HackerNewsUsersApiImpl extends HackerNewsUsersApi {

  override def getUserJson(userId: String): String = {
    if(userId.isEmpty)
      throw new IllegalArgumentException

    val url: URL = new URL(
      s"https://hacker-news.firebaseio.com/v0/user/$userId.json?print=pretty")
    val textSource = Source.fromURL(url)
    val response: String = textSource.mkString

    textSource.close()
    response
  }
}
