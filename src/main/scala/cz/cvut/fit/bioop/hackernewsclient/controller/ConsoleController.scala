package cz.cvut.fit.bioop.hackernewsclient.controller

import cz.cvut.fit.bioop.hackernewsclient.controller.ConsoleController.{API_REQUEST_FAIL_TEXT, CLEAR_CACHE_TEXT, HELP_TEXT, cantFindUserMessage, couldNotFetchStoryMessage, noCommentsMessage, setPageSizeMessage}
import cz.cvut.fit.bioop.hackernewsclient.repository._
import cz.cvut.fit.bioop.hackernewsclient.utils.exception._
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.FormatExtension._

/**
 * Controller for HackersNews console client. Methods sends string responses to the view.
 * @param storiesRepository repository which handles operations with stories data.
 * @param usersRepository repository which handles operations with users data.
 */
class ConsoleController(
  storiesRepository: StoriesRepository,
  commentsRepository: CommentsRepository,
  usersRepository: UserRepository
) extends Controller[String] {

  override def showHelp(): String = HELP_TEXT

  override def showTopStories(page: Int = 1): String = {
    val pageIndex = page - 1
    val storiesToShow =
      storiesRepository.getTopStoriesPage(pageIndex, _pageSize)
    storiesToShow.formatIndexed(pageIndex * _pageSize + 1)
  }

  override def showNewStories(page: Int = 1): String = {
    val pageIndex = page - 1
    val storiesToShow =
      storiesRepository.getNewStoriesPage(pageIndex, _pageSize)
    storiesToShow.formatIndexed(pageIndex * _pageSize + 1)
  }

  override def showUserDetail(userId: String): String =
    try usersRepository.getUser(userId).format()
    catch {
      case _: ApiRequestFailed => API_REQUEST_FAIL_TEXT
      case _: InvalidUserIdException => cantFindUserMessage(userId)
    }

  override def showCommentsById(storyId: Long, page: Int): String = {
    val pageIndex = page - 1
    val story = storiesRepository.getStory(storyId)

    if(story.isEmpty) {
      return couldNotFetchStoryMessage(storyId)
    }

    val commentsList = commentsRepository.getStoryCommentsPage(story.get, pageIndex, _pageSize)
    if (commentsList.isEmpty)
      noCommentsMessage(storyId,page)
    else {
      commentsList.format(page)
    }
  }

  override def setPageSize(pageSize: Int): String = {
    _pageSize = pageSize
    setPageSizeMessage(pageSize)
  }

  override def clearCache(): String = {
    storiesRepository.clearCache()
    commentsRepository.clearCache()
    CLEAR_CACHE_TEXT
  }
}

object ConsoleController {
  val HELP_TEXT = "Supported commands: help, top-stories, new-stories, user, comments, set-page-size, clear-cache, exit, stop"
  val API_REQUEST_FAIL_TEXT = "# Api request failed"
  val CLEAR_CACHE_TEXT = s" ~ Cache were cleared!"
  def cantFindUserMessage(userId: String) = s"Could not find user with id: $userId"
  def setPageSizeMessage(pageSize: Int) = s" ~ Page size set to $pageSize"
  def noCommentsMessage(storyId: Long, page: Int) = s"No comments found for id $storyId page $page!"
  def couldNotFetchStoryMessage(storyId: Long) = s"Could not fetch story with id $storyId"
}
