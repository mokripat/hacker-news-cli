package cz.cvut.fit.bioop.hackernewsclient.model.command

sealed trait Command

object Command {
  case class NewStories(page: Int = 1) extends Command
  case class TopStories(page: Int = 1) extends Command
  case class User(userId: String) extends Command
  case class Comments(storyId: Long, page: Int = 1) extends Command
  case class SetPageSize(pageSize: Int) extends Command
  case class WrongArgs(message: String) extends Command
  case object ClearCache extends Command
  case object Help extends Command
  case object Exit extends Command
  case object Unknown extends Command
}
