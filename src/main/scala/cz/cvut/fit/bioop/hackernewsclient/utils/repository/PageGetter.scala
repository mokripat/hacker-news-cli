package cz.cvut.fit.bioop.hackernewsclient.utils.repository

/**
 * Serves as item list slicer. By given parameters slices chunk from list by given parameters
 * which is called page and its represented as list.
 */
object PageGetter {
  /**
   * Determines which chunk of items is wanted.
   * @param items list containing all items.
   * @param pageIndex index of wanted page.
   * @param countPerPage number of stories per page.
   * @return page (list) of items by given index.
   */
  def getPageFromItems[T](
                      items: List[T],
                      pageIndex: Int,
                      countPerPage: Int): List[T] = {
    val pageStartIndex = pageIndex * countPerPage

    if (items.size - 1 < pageStartIndex)
      List()
    else
      items.splitAt(pageStartIndex)._2.take(countPerPage)
  }
}
