package cz.cvut.fit.bioop.hackernewsclient.repository

import cz.cvut.fit.bioop.hackernewsclient.api.item.HackerNewsCommentsApi
import cz.cvut.fit.bioop.hackernewsclient.database.proxy.DatabaseCacheProxy
import cz.cvut.fit.bioop.hackernewsclient.model.item.{Comment, Story}
import cz.cvut.fit.bioop.hackernewsclient.utils.Constants.DEFAULT_PAGE_SIZE
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter
import cz.cvut.fit.bioop.hackernewsclient.utils.repository.PageGetter.getPageFromItems

import scala.collection.mutable.ListBuffer

/**
 * Repository for HackerNews comments.
 */
trait CommentsRepository {
  /**
   * Fetches, formats and returns page of comments for given story.
   * @param story of which comments are wanted.
   * @param pageIndex index of wanted page.
   * @param pageSize size of one page.
   * @return [pageIndex]-th page with [pageSize] size of comments for given story.
   */
  def getStoryCommentsPage(
    story: Story,
    pageIndex: Int,
    pageSize: Int = DEFAULT_PAGE_SIZE): List[Comment]

  /**
   * Erases all current persisted data.
   */
  def clearCache(): Unit
}

/**
 * Implementation of CommentsRepository.
 *
 * @param commentsApi Api for fetching comments in raw form (json).
 * @param commentsDatabaseProxy Proxy connected to database to persists and handle comments caches.
 * @param jsonFormatter Formatter class to transform raw data to HackerNewsItems.
 */
class CommentsRepositoryImpl(
  commentsApi: HackerNewsCommentsApi,
  commentsDatabaseProxy: DatabaseCacheProxy[Long, Comment],
  jsonFormatter: JsonFormatter
) extends CommentsRepository {

  override def getStoryCommentsPage(
    story: Story,
    pageIndex: Int,
    pageSize: Int = DEFAULT_PAGE_SIZE): List[Comment] =
      getStoryCommentsFromIds(
        getPageFromItems(
          story.kids,
          pageIndex,
          pageSize
        ))

  /**
   * Tries to get all comments by given IDs, if found in database then loads, else fetches
   * @param commentsIds list containing stories IDs.
   * @return list of comments with given ids.
   */
  private def getStoryCommentsFromIds(commentsIds: List[Long]): List[Comment] = {
    val commentsBuffer = new ListBuffer[Comment]

    commentsIds
      .foreach( commentId =>
        commentsDatabaseProxy.get(commentId) match {
          case Some(value) => commentsBuffer.addOne(value)
          case None =>
            val rawComment = commentsApi.getComment(commentId)
            val formattedComment = jsonFormatter.itemJsonToComment(rawComment)

            if(formattedComment.nonEmpty) {
              commentsDatabaseProxy.save(commentId,formattedComment.get)
              commentsBuffer.addOne(formattedComment.get)
            }

        }
      )

    commentsBuffer.toList
  }

  override def clearCache(): Unit = commentsDatabaseProxy.clear()
}
