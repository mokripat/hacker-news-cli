package cz.cvut.fit.bioop.hackernewsclient.utils.parser

trait StringParser[V] extends Parser[String, V] {
  override def parse(input: String): V
}
