package cz.cvut.fit.bioop.hackernewsclient.model.item
/**
 * Model for HackerNews item, which serves as comment to ask, poll or story.
 *
 * @param by The username of the item's author.
 * @param itemId The item's unique id.
 * @param time Creation date of the item, in Unix Time.
 * @param parentId id of comment parent.
 * @param text The comment text. HTML.
 * @param itemType The type of item.
 * @param kids The ids of the item's comments, in ranked display order.
 */
case class Comment(
  itemId: Long,
  by: String,
  time: Long,
  parentId: Long,
  text: String,
  itemType: String,
  kids: List[Long] = List(),
)

