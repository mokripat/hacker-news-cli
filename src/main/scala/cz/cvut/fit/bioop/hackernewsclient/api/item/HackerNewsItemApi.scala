package cz.cvut.fit.bioop.hackernewsclient.api.item

import cz.cvut.fit.bioop.hackernewsclient.api.item.HackerNewsItemApi.itemURL

import java.io.IOException
import java.net.URL
import scala.io.Source

/**
 * Base class for fetching raw resource of any HackerNews item in json.
 */
class HackerNewsItemApi {

  /**
   * Fetch an item in json by given item ID from HackerNews.
   * @param itemId id of the item.
   * @return raw resource of item by given ID.
   */
  def getItem(itemId: Long): String =
    try {
      val url: URL = new URL(itemURL(itemId))
      val textSource = Source.fromURL(url)
      val response: String = textSource.mkString

      textSource.close()
      response
    } catch {
      case _: IOException => ""
    }
}

object HackerNewsItemApi {
  def itemURL(itemId: Long) =
    s"https://hacker-news.firebaseio.com/v0/item/$itemId.json?print=pretty"
}
