package cz.cvut.fit.bioop.hackernewsclient.utils.exception

import java.io.IOException

/**
 * Summarizing exceptions thrown by calling API requests.
 */
class ApiRequestFailed extends IOException
