package cz.cvut.fit.bioop.hackernewsclient.utils.exception

/**
 * Exception when user has not been found by given ID.
 */
class InvalidUserIdException extends Exception
