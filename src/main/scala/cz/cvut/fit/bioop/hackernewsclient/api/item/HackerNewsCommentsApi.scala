package cz.cvut.fit.bioop.hackernewsclient.api.item

/**
 * Serves for fetching raw comments data (json) from HackerNews API.
 */
trait HackerNewsCommentsApi {
  /**
   * Fetches item comments with given IDs.
   * @param kids IDs of item kids.
   * @return list of kids raw resource.
   */
  def getComments(kids: List[Long]): List[String]

  /**
   * Fetches comment by given ID.
   * @param commentId ID of wanted comment
   * @return raw resource of comment.
   */
  def getComment(commentId: Long): String
}

/**
 * @param itemApi for fetching single comment + testability.
 */
class HackerNewsCommentsApiImpl(itemApi: HackerNewsItemApi) extends HackerNewsCommentsApi {

  override def getComments(kids: List[Long]): List[String] =
    kids.map(commentId => getComment(commentId))

  override def getComment(commentId: Long): String = itemApi.getItem(commentId)
}
