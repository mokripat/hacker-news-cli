package cz.cvut.fit.bioop.hackernewsclient.utils.parser

trait Parser[U,V] {
  def parse(input: U): V
}
