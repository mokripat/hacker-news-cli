package cz.cvut.fit.bioop.hackernewsclient.database

/**
 * General database for persisting data.
 * @tparam U type of items identifier.
 * @tparam V type of items we want to store.
 */
trait Database[U,V] {
  /**
   * Get item from database.
   * @param itemId identifier of wanted item.
   * @return Some[U] if item was found of None if item was not found
   */
  def get(itemId: U) : Option[V]

  /**
   * Persists item which can be looked up with given ID.
   * @param itemId id by which can be item found.
   * @param item wanted to persists.
   * @return true if was successfully persisted or false if there is item already with given ID.
   */
  def save(itemId: U, item: V) : Boolean

  /**
   * Remove item from persisted data.
   * @param itemId id of item.
   * @return true if item was successfully removed, false otherwise.
   */
  def remove(itemId: U) : Boolean

  /**
   * Look up for item by given ID and updates if it was found.
   * @param itemId id by which can be item found.
   * @param item update data.
   * @return true if item was found and updated, false otherwise.
   */
  def update(itemId: U, item: V) : Boolean

  /**
   * Clears all persisted data.
   */
  def clear() : Unit

  /**
   * @return how many items are currently persisted.
   */
  def size : Int
}
