package cz.cvut.fit.bioop.hackernewsclient.repository

import cz.cvut.fit.bioop.hackernewsclient.api.user.HackerNewsUsersApi
import cz.cvut.fit.bioop.hackernewsclient.model.user.HackerNewsUser
import cz.cvut.fit.bioop.hackernewsclient.utils.exception.{ApiRequestFailed, InvalidUserIdException}
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter

import java.io.IOException

/**
 * Repository for HackerNews user.
 */
trait UserRepository {

  /**
   * Fetches, formats and returns user by given user ID.
   * @param userId ID of wanter user.
   * @throws InvalidUserIdException if could not fetch user by given userId or NetworkException if API request failed.
   * @return [HackerNewsUser] with given ID.
   */
  def getUser(userId: String): HackerNewsUser
}

/**
 * Implementation of UserRepository.
 * @param usersApi Api for fetching user data in raw form (json).
 * @param jsonFormatter Formatter class to transform raw data to HackerNewsStory
 */
class UserRepositoryImpl(
  usersApi: HackerNewsUsersApi,
  jsonFormatter: JsonFormatter,
) extends UserRepository {

  override def getUser(userId: String): HackerNewsUser = {
    val rawUser = try usersApi.getUserJson(userId)
    catch {
      case _: IOException => throw new ApiRequestFailed
    }
    val userOption = jsonFormatter.userJsonToUser(rawUser)

    if (userOption.isEmpty)
      throw new InvalidUserIdException
    else
      userOption.get
  }
}
