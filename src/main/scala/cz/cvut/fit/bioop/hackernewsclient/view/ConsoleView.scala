package cz.cvut.fit.bioop.hackernewsclient.view

import cz.cvut.fit.bioop.hackernewsclient.controller.Controller
import cz.cvut.fit.bioop.hackernewsclient.model.command.Command
import cz.cvut.fit.bioop.hackernewsclient.utils.parser.StringParser

import scala.io.StdIn

/**
 * Console view which sends successfully parsed user input to controller and prints controller response.
 * @param controller controller which handles read commands and answers with string response.
 * @param stringParser parses which takes user input and returns command which should be processed.
 */
class ConsoleView(controller: Controller[String], stringParser: StringParser[Command]) {

  private var running = false

  /**
   * Starts reading user input and executing commands.
   */
  def run(): Unit = {
    running = true

    println("Listening now.. Enter command:")

    while (running) {
      print("> ")
      //read from input
      val command = StdIn.readLine()

      processCommand(command)
    }
  }

  /**
   * Stops this view.
   */
  def stop(): Unit =
    running = false

  /**
   * Serves for observing if view is still running
   * @return if the view is currently running.
   */
  def isRunning: Boolean = running

  /**
   * Parses user input, sends it to controller and prints response.
   * @param command The user input.
   */
  def processCommand(command: String): Unit = {
    println(
      stringParser.parse(command) match {
        case Command.TopStories(page) => controller.showTopStories(page)
        case Command.NewStories(page) => controller.showNewStories(page)
        case Command.User(userId) => controller.showUserDetail(userId)
        case Command.Comments(storyId, page) => controller.showCommentsById(storyId, page)
        case Command.SetPageSize(pageSize) => controller.setPageSize(pageSize)
        case Command.WrongArgs(message) => message
        case Command.ClearCache => controller.clearCache()
        case Command.Help => controller.showHelp()
        case Command.Unknown => "I don't understand. Type help to see supported commands."
        case Command.Exit =>
          stop()
          "Exiting.."
    })
  }
}