package cz.cvut.fit.bioop.hackernewsclient.api.item

import java.io.IOException
import java.net.URL
import scala.io.Source

/**
 * Serves for fetching raw stories data (json) from HackerNews API.
 */
trait HackerNewsStoriesApi {

  /**
   * Fetch a story in json by given story ID from HackerNews.
   * @param storyId id of the story.
   * @return raw resource of story by given ID.
   */
  def getStory(storyId: Long): String

  /**
   * Fetches current top stories ids using endpoint /v0/topstories.
   * @return raw resource of top stories IDs.
   */
  def getTopStoriesIdsBox: String

  /**
   * Fetches latest stories ids using endpoint /v0/newstories.
   * @return raw resource of new stories IDs.
   */
  def getNewStoriesIdsBox: String
}

/**
 * @param itemApi for fetching single comment + testability.
 */
class HackerNewsStoriesApiImpl(itemApi: HackerNewsItemApi) extends HackerNewsStoriesApi {

  override def getStory(storyId: Long): String = itemApi.getItem(storyId)

  override def getTopStoriesIdsBox: String =
    try {
      val url: URL = new URL(HackerNewsStoriesApi.topStoriesURL)
      val textSource = Source.fromURL(url)
      val response: String = textSource.mkString

      textSource.close()
      response
    } catch {
      case _: IOException => "[]"
    }

  override def getNewStoriesIdsBox: String =
    try {
      val url: URL = new URL(HackerNewsStoriesApi.newStoriesURL)
      val responseSource = Source.fromURL(url)
      val response: String = responseSource.mkString

      responseSource.close()
      response
    } catch {
      case _: IOException => "[]"
    }
}

object HackerNewsStoriesApi {
  val topStoriesURL = s"https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty"
  val newStoriesURL = s"https://hacker-news.firebaseio.com/v0/newstories.json?print=pretty"
}
