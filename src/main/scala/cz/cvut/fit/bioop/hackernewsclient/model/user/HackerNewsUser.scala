package cz.cvut.fit.bioop.hackernewsclient.model.user

/**
 * Model for HackerNews user.
 *
 * @param id ID of user.
 * @param created Unix time in seconds when account was created.
 * @param karma number of upvotes obtained across all posts.
 * @param about description of user written by that user.
 * @param submitted number of submitted posts.
 */
case class HackerNewsUser(
  id: String,
  created: Long,
  karma: Int,
  about: String,
  submitted: List[Long])
