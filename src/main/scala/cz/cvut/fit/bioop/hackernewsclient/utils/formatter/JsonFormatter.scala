package cz.cvut.fit.bioop.hackernewsclient.utils.formatter

import cz.cvut.fit.bioop.hackernewsclient.model.item
import cz.cvut.fit.bioop.hackernewsclient.model.item.{Comment, Story}
import cz.cvut.fit.bioop.hackernewsclient.model.user.HackerNewsUser
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.FormatExtension.HtlmUnescape
import ujson.Value
import upickle.default._

import java.util.NoSuchElementException
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class JsonFormatter {
  /**
   * Converts string version of json array to list.
   * @param jsonBox raw list json data in String.
   * @return Some(list) of given data if was successfully parsed.
   */
  def jsonBoxToList(jsonBox: String): Option[List[Long]] =
    try {
      Some(read[List[Long]](jsonBox))
    } catch {
      case _: Exception => None
    }

  /**
   * Creates instance of HackerNews story from json stored in String if possible else returns empty Option.
   * @param storyJson string version of HackerStory json.
   * @return Option with instance of story or none.
   */
  def itemJsonToStory(storyJson: String): Option[Story] = {
    try {
      val data = ujson.read(storyJson)

      val itemType = data("type").str

      if (itemType != "story")
        throw new IllegalArgumentException

      /**
       * Mandatory arguments.
       */
      val by = data("by").str
      val id = data("id").num.toLong
      val score = data("score").num.toInt
      val time = data("time").num.toLong
      val title = data("title").str.unescape()

      /**
       * Story does not necessarily need to contain url.
       */
      val url = tryStringOrEmpty(data, "url")

      /**
       * Descendants and kids are optional parameters.
       */
      try {
        val descendants = data("descendants").num.toInt
        val kidsNumBuffer = data("kids").arr
          .asInstanceOf[ArrayBuffer[ujson.Num]]

        /** converting ArrayBuffer[ujson.Num] to ArrayBuffer[Long] */
        val kidsBuffer = new ListBuffer[Long]
        kidsNumBuffer.foreach(kid => kidsBuffer.addOne(kid.value.toLong))

        Option(
          Story(
            by,
            id,
            time,
            score,
            title,
            url,
            itemType,
            descendants,
            kidsBuffer.toList))
      } catch {
        case _: NoSuchElementException =>
          Option(
            item.Story(by, id, time, score, title, url, itemType))
      }

    } catch {
      case _: Exception => Option.empty
    }
  }

  /**
   * Creates instance of HackerNews comment from json stored in String if possible else returns empty Option.
   * @param commentJson string version of HackerStory json.
   * @return Option with instance of comment or none.
   */
  def itemJsonToComment(commentJson: String): Option[Comment] = {
    try {
      val data = ujson.read(commentJson)

      /**
       * Mandatory arguments.
       */
      val by = data("by").str
      val id = data("id").num.toLong
      val parent = data("parent").num.toLong
      val text = data("text").str.unescape()
      val time = data("time").num.toLong
      val itemType = data("type").str

      val kidsNumBuffer =
        try data("kids").arr.asInstanceOf[ArrayBuffer[ujson.Num]]
        catch {
          case _: NoSuchElementException => new ArrayBuffer[ujson.Num]
        }

      /** converting ArrayBuffer[ujson.Num] to ArrayBuffer[Long] */
      val kidsBuffer = new ListBuffer[Long]
      kidsNumBuffer.foreach(kid => kidsBuffer.addOne(kid.value.toLong))

      Option(
        Comment (
          id,
          by,
          time,
          parent,
          text,
          itemType,
          kidsBuffer.toList
        )
      )
    } catch {
      case _: Exception => Option.empty
    }
  }

  /**
   * Creates instance of HackerNews user from json stored in String if possible else returns empty Option.
   * @param userJson string version of HackerStory json.
   * @return Option with instance of story or none.
   */
  def userJsonToUser(userJson: String): Option[HackerNewsUser] = {
    try {
      val data = ujson.read(userJson)

      /**
       * Mandatory arguments.
       */
      val id = data("id").str
      val created = data("created").num.toLong
      val karma = data("karma").num.toInt

      /**
       * User does not necessarily need to have "about" filled.
       */
      val about = tryStringOrEmpty(data, "about").unescape()

      val _submittedBuffer: ArrayBuffer[ujson.Num] = try data("submitted").arr
        .asInstanceOf[ArrayBuffer[ujson.Num]]
      catch {
        case _: NoSuchElementException => new ArrayBuffer[ujson.Num]
      }

      val submittedBuffer = new ListBuffer[Long]
      _submittedBuffer.foreach(kid => submittedBuffer.addOne(kid.value.toLong))

      Option(HackerNewsUser(id, created, karma, about, submittedBuffer.toList))
    } catch {
      case _: Exception => Option.empty
    }
  }

  private def tryStringOrEmpty(data: Value.Value, elementName: String) : String = {
    try {
      data(elementName).str
    } catch {
      case _: NoSuchElementException => ""
    }
  }
}
