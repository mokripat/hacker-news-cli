package cz.cvut.fit.bioop.hackernewsclient.model.item

/**
 * Base(model) class for HackerNews items with shared required parameters.
 */
abstract class HackerNewsItem {
  /** The username of the item's author. */
  val by: String
  /** The item's unique id. */
  val itemId: Long
  /** Creation date of the item, in Unix Time. */
  val time: Long
  /** The item's type (sent by HW Api) */
  val itemType : String
}
