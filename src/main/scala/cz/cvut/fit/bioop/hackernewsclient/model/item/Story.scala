package cz.cvut.fit.bioop.hackernewsclient.model.item

/**
 * Model for HackerNews story item.
 *
 * @param by The username of the item's author.
 * @param itemId The item's unique id.
 * @param time Creation date of the item, in Unix Time.
 * @param score The ask upvotes.
 * @param title The title of the ask.
 * @param url URL to full article.
 * @param itemType The type of item.
 * @param descendants The total comment count.
 * @param kids The ids of the item's comments, in ranked display order.
 */
case class Story(
  by: String,
  itemId: Long,
  time: Long,
  score: Int,
  title: String,
  url: String,
  itemType: String,
  descendants: Int = 0,
  kids: List[Long] = List())
    extends HackerNewsItem
