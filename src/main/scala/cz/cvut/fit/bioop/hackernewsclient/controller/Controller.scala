package cz.cvut.fit.bioop.hackernewsclient.controller

import cz.cvut.fit.bioop.hackernewsclient.utils.Constants

/**
 * Controller for HackersNews client.
 * @tparam T determines object type for communication between controller and view.
 */
trait Controller[T] {
  /**
   * Only readable property which indicates current set page size when browsing stories or comments.
   */
  def pageSize: Int = _pageSize

  /**
   * Internal property to manipulate with page size.
   */
  protected var _pageSize: Int = Constants.DEFAULT_PAGE_SIZE

  /**
   * Shows a help with commands user can use.
   */
  def showHelp(): T

  /**
   * Presents current top stories to the user.
   * @param page index of wanted page.
   */
  def showTopStories(page: Int = 1): T

  /**
   * Presents current new stories to the user.
   * @param page index of wanted page.
   */
  def showNewStories(page: Int = 1): T

  /**
   * Tries to find and present user by given ID.
   * @param userId ID of wanted user.
   */
  def showUserDetail(userId: String): T

  /**
   * Shows comments of story by given ID.
   * @param storyId ID of story's comments are wanted.
   * @param page wanted page.
   */
  def showCommentsById(storyId: Long, page: Int): T

  /**
   * Erases all current persisted data.
   */
  def clearCache(): T

  /**
   * Sets size of pages when displaying list of stories or comments.
   * @param pageSize wanted size of pages.
   */
  def setPageSize(pageSize: Int): T
}
