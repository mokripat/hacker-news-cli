package cz.cvut.fit.bioop.hackernewsclient.utils.formatter

import cz.cvut.fit.bioop.hackernewsclient.model.item.{Comment, Story}
import cz.cvut.fit.bioop.hackernewsclient.model.user.HackerNewsUser

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId}
import scala.io.AnsiColor._

/**
 * Extension methods for HackerNews objects for formatting their model to string.
 */
object FormatExtension {

  implicit class UserFormatter(user: HackerNewsUser) {
    def format(): String = {
      val formattedAbout = if (user.about.nonEmpty) s" | ${user.about}" else ""
      val date = Instant
        .ofEpochSecond(user.created)
        .atZone(ZoneId.systemDefault())
        .toLocalDate
        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))

      s"ID: $BOLD${user.id}$RESET$formattedAbout\n  # $GREEN${user.karma}$RESET karma with $BLUE${user.submitted.size}$RESET submission, created on $date"
    }
  }

  implicit class StoryFormatter(story: Story) {
    def format(): String = {
      s"# $BOLD${story.title}$RESET ($BLUE$UNDERLINED${story.url}$RESET)\n" +
        s"   $YELLOW[by $RED${story.by}$RESET$YELLOW with $GREEN${story.score}$RESET$YELLOW upvotes | ${story.descendants} comments | id: ${story.itemId}]$RESET"
    }
  }

  implicit class StoriesListFormatter(storiesList: List[Story]) {
    def formatIndexed(firstIndex: Int): String = {
      var index = firstIndex
      var result = ""
      storiesList.foreach(story => {
        result += s"$BOLD$index$RESET${story.format()}\n"
        index += 1
      })
      result.dropRight(1)
    }
  }

  implicit class CommentFormatter(comment: Comment)  {
    def format(): String =
      s"^ ${comment.text}\n" +
        s"   $YELLOW[by $RED${comment.by}$RESET$YELLOW | ${comment.kids.size} comments]$RESET"

    //TODO use if multilevel comment browsing is implemented
    def formatWithId(): String =
      s"^ ${comment.text}\n" +
        s"   $YELLOW[by $RED${comment.by}$RESET$YELLOW | ${comment.kids.size} comments | id: ${comment.itemId}]$RESET"
  }

  implicit class CommentsListFormatter(commentsList: List[Comment]) {
    def format(page: Int = 1): String = {
      var result = ""
      result += s"${RED_B}Comments (page $page):$RESET\n"

      commentsList.foreach(comment => {
        result += s"${comment.format()}\n"
      })
      result.dropRight(1)
    }
  }

  implicit class HtlmUnescape(htmlText: String) {
    /**
     * Applies all other defined html unescape methods.
     */
    def unescape(): String = {
      htmlText
        .unescapeHexCharacters()
        .unescapeReservedCharacters()
        .unescapeParagraphs()
        .unescapeHrefLinks()
        .unescapeItalics()
        .unescapeBold()
    }

    /**
     * Erases all <p> and replaces </p> with newline.
     */
    def unescapeParagraphs(): String = {
      htmlText
        .replaceAll("<p>","")
        .replaceAll("</p>","\n")
    }

    /**
     * Finds all <a href="link">link</a> links and replace them with (link)
     */
    def unescapeHrefLinks(): String = {
      var result = htmlText
      val hrefLinksMatcher = "<a href=[^<]*</a>".r

      val hrefLinks = (hrefLinksMatcher findAllMatchIn htmlText).toSet

      hrefLinks.foreach( hrefLink => {
        val linkMatcher = ">.*</a>".r
        val linkOpt = linkMatcher findFirstMatchIn hrefLink.toString()
        val link = linkOpt.get.toString().replaceAll("</a>","").replace(">", "")

        result = result.replaceAll(hrefLink.toString(), link)
      })

      result
    }

    /**
     * DISCLAIMER: ITALICS IS NOT IN GENERAL ANSI ESCAPE CODES FOR SCALA SO I USE BLACK COLOR INSTEAD
     * Finds all <i> or <em> tags and changes text inside them to black
     */
    def unescapeItalics(): String = {
      var result = htmlText
      val italicsMatcher = "(<i>[^<]*</i>|<em>[^<]*</em>)".r

      val italics = (italicsMatcher findAllMatchIn htmlText).toSet

      italics.foreach( italic => {
        val textMatcher = ">.*</(i|em)>".r
        val text = textMatcher findFirstMatchIn italic.toString()
        val italicsText = text.get.toString().replaceAll("</(i|em)>",s"$RESET").replace(">", s"$BLACK")

        result = result.replaceAll(italic.toString(), italicsText)
      })

      result
    }

    /**
     * Finds all <b> or <strong> tags and changes text inside them to black
     */
    def unescapeBold(): String = {
      var result = htmlText
      val boldMatcher = "(<b>[^<]*</b>|<strong>[^<]*</strong>)".r

      val bolds = (boldMatcher findAllMatchIn htmlText).toSet

      bolds.foreach( bold => {
        val textMatcher = ">.*</(b|strong)>".r
        val text = textMatcher findFirstMatchIn bold.toString()
        val italicsText = text.get.toString().replaceAll("</(b|strong)>",s"$RESET").replace(">", s"$BOLD")

        result = result.replaceAll(bold.toString(), italicsText)
      })

      result
    }

    /**
     * Unescapes all html hex characters in format &#____; to unicode characters.
     */
    def unescapeHexCharacters(): String = {
      var result = htmlText
      val hexCharactersMatcher = "&#.{3,4};".r

      val hexCharacters = (hexCharactersMatcher findAllMatchIn htmlText).toSet

      hexCharacters.foreach( hexForm => {
        val uniCodeMatcher = "([A-Fa-f0-9]+)".r
        val unicodeCodeOpt = uniCodeMatcher findFirstMatchIn hexForm.toString()
        val unicodeCode = unicodeCodeOpt.get.toString()

        val hexVal = Integer.parseInt(unicodeCode, 16)
        result = result.replaceAll(hexForm.toString(), hexVal.toChar.toString)
      })

      result
    }

    /**
     * Exchange all HTML reserved character codes with their UTF character.
     */
    def unescapeReservedCharacters(): String = {
      var result = htmlText
      val reservedCharactersTranslation = Map (
        "&quot;" -> "\"",
        "&apos;" -> "'",
        "&amp;" -> "&",
        "&lt;" -> "<",
        "&gt;" -> ">"
      )

      for ((htmlCode,translation) <- reservedCharactersTranslation) {
        result = result.replaceAll(htmlCode, translation)
      }
      result
    }
  }
}
