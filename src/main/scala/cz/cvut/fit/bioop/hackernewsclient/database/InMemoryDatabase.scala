package cz.cvut.fit.bioop.hackernewsclient.database

/**
 * Database which holds data in memory in runtime.
 * @tparam U type of items identifier.
 * @tparam V type of items we want to store.
 */
class InMemoryDatabase[U,V] extends Database[U,V] {

  protected var data : Map[U,V] = Map()

  override def get(itemId: U): Option[V] = data.get(itemId)

  override def save(itemId: U, item: V): Boolean = {
    if(get(itemId).nonEmpty) {
      false
    } else {
      data += (itemId -> item)
      true
    }
  }

  override def remove(itemId: U): Boolean =
    if(get(itemId).nonEmpty) {
      data = data - itemId
      true
    } else
      false

  override def update(itemId: U, item: V): Boolean = {
    if(get(itemId).nonEmpty) {
      data += (itemId -> item)
      true
    } else
      false
  }

  override def size: Int = data.size

  /**
   * Clears all persisted data.
   */
  override def clear(): Unit = {
    data = Map()
  }
}
