package cz.cvut.fit.bioop.hackernewsclient.repository

import cz.cvut.fit.bioop.hackernewsclient.api.item.HackerNewsStoriesApi
import cz.cvut.fit.bioop.hackernewsclient.database.proxy.DatabaseCacheProxy
import cz.cvut.fit.bioop.hackernewsclient.model.item.Story
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter
import org.mockito.Mockito.never
import org.mockito.MockitoSugar.{mock, reset, verify, when}
import org.scalatest.funsuite.AnyFunSuite

class StoriesRepositoryImplTest extends AnyFunSuite {
  val storiesApi: HackerNewsStoriesApi = new TestApi()
  val databaseProxy: DatabaseCacheProxy[Long,Story] = mock[DatabaseCacheProxy[Long,Story]]
  val jsonFormatter: JsonFormatter = mock[JsonFormatter]

  val testStoriesRepository = new StoriesRepositoryImpl(storiesApi,databaseProxy,jsonFormatter)

  private val topStoriesIdBox = "[1,2,3]"
  private val topStoriesIdList = List(1L,2L,3L)
  private val newStoriesIdBox = "[3,2,1]"
  private val newStoriesIdList = List(3L,2L,1L)
  private val rawStory1 = "rawStory1"
  private val rawStory2 = "rawStory2"
  private val rawStory3 = "rawStory3"
  private val testStory1 = Story("admin",1,Long.MaxValue,256,"first story","admin.root","story",2,List(4,5))
  private val testStory2 = Story("pepa",2,Long.MaxValue,256,"second story","admin.root","story",3,List(16,17,18))
  private val testStory3 = Story("vladimir",3,Long.MaxValue,256,"third story","admin.root","story",0,List(0))

  test("get 1 top story out of 3 total from api, with nothing in proxy yet") {
    beforeTest()
    when(databaseProxy.get(testStory1.itemId)) thenReturn None

    assert(testStoriesRepository.getTopStories(1) == List(testStory1))
    verify(databaseProxy).save(testStory1.itemId,testStory1)
  }

  test("get 1 top story out of 3 total from api, with already cached in proxy") {
    beforeTest()
    when(databaseProxy.get(testStory1.itemId)) thenReturn Some(testStory1)

    assert(testStoriesRepository.getTopStories(1) == List(testStory1))
  }

  test("get first page with size 2 of top stories") {
    beforeTest()
    when(databaseProxy.get(testStory1.itemId)) thenReturn Some(testStory1)
    when(databaseProxy.get(testStory2.itemId)) thenReturn Some(testStory2)

    assert(testStoriesRepository.getTopStoriesPage(0,2) == List(testStory1,testStory2))
  }

  test("request for stories out of range") {
    beforeTest()
    assert(testStoriesRepository.getTopStoriesPage(420,420) == List())
  }

  test("request for stories with api issue") {
    beforeTest()
    //there should be used some invalid box of IDs but for simplicity just change the return
    when(jsonFormatter.jsonBoxToList("[1,2,3]")) thenReturn None

    assert(testStoriesRepository.getTopStoriesPage(0) == List())
  }

  test("request for stories with on the edge") {
    beforeTest()
    when(databaseProxy.get(testStory3.itemId)) thenReturn Some(testStory3)

    assert(testStoriesRepository.getTopStoriesPage(1,2) == List(testStory3))
  }

  test("get 1 new story") {
    beforeTest()
    when(databaseProxy.get(testStory3.itemId)) thenReturn Some(testStory3)

    assert(testStoriesRepository.getNewStories(1) == List(testStory3))
  }

  test("get first page with size 2 of new stories ") {
    beforeTest()
    when(databaseProxy.get(testStory3.itemId)) thenReturn Some(testStory3)
    when(databaseProxy.get(testStory2.itemId)) thenReturn Some(testStory2)

    assert(testStoriesRepository.getNewStoriesPage(0,2) == List(testStory3,testStory2))
  }

  test("invalid story raw resource") {
    beforeTest()
    val randomNotWorkingId = 42
    when(databaseProxy.get(randomNotWorkingId)) thenReturn None
    when(jsonFormatter.itemJsonToStory("")) thenReturn None

    assert(testStoriesRepository.getStory(randomNotWorkingId).isEmpty)
    verify(databaseProxy, never).save(randomNotWorkingId,_)
  }

  test("some invalid stories") {
    beforeTest()
    when(databaseProxy.get(testStory1.itemId)) thenReturn None
    when(databaseProxy.get(testStory2.itemId)) thenReturn None
    when(databaseProxy.get(testStory3.itemId)) thenReturn None

    when(jsonFormatter.itemJsonToStory(rawStory1)) thenReturn Some(testStory1)
    when(jsonFormatter.itemJsonToStory(rawStory2)) thenReturn None
    when(jsonFormatter.itemJsonToStory(rawStory3)) thenReturn None

    assert(testStoriesRepository.getTopStories(3) == List(testStory1))
    verify(databaseProxy).save(testStory1.itemId,testStory1)
  }

  test("clear cache") {
    beforeTest()
    testStoriesRepository.clearCache()
    verify(databaseProxy).clear()
  }


  private def beforeTest(): Unit = {
    reset(databaseProxy)
    reset(jsonFormatter)

    when(jsonFormatter.jsonBoxToList(topStoriesIdBox)) thenReturn Some(topStoriesIdList)
    when(jsonFormatter.jsonBoxToList(newStoriesIdBox)) thenReturn Some(newStoriesIdList)

    when(jsonFormatter.itemJsonToStory(rawStory1)) thenReturn Option(testStory1)
    when(jsonFormatter.itemJsonToStory(rawStory2)) thenReturn Option(testStory2)
    when(jsonFormatter.itemJsonToStory(rawStory3)) thenReturn Option(testStory3)

    when(databaseProxy.save(1, testStory1)) thenReturn true
    when(databaseProxy.save(2, testStory2)) thenReturn true
    when(databaseProxy.save(3, testStory3)) thenReturn true
  }

  class TestApi extends HackerNewsStoriesApi {
    override def getTopStoriesIdsBox: String = "[1,2,3]"
    override def getNewStoriesIdsBox: String = "[3,2,1]"
    override def getStory(itemId: Long): String = itemId match {
      case 1 => rawStory1
      case 2 => rawStory2
      case 3 => rawStory3
      case _ => ""
    }
  }
}
