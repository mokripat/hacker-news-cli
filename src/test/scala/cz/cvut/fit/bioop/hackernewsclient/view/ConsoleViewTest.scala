package cz.cvut.fit.bioop.hackernewsclient.view

import cz.cvut.fit.bioop.hackernewsclient.controller.Controller
import cz.cvut.fit.bioop.hackernewsclient.model.command.Command
import cz.cvut.fit.bioop.hackernewsclient.utils.parser.StringParser
import org.mockito.MockitoSugar.{mock, reset, verify, verifyZeroInteractions, when}
import org.scalatest.funsuite.AnyFunSuite

class ConsoleViewTest extends AnyFunSuite {
  val testController: Controller[String] = mock[Controller[String]]
  val testStringParser: StringParser[Command] = mock[StringParser[Command]]
  val testConsoleView = new ConsoleView(testController,testStringParser)

  val dummyInput = "dummy"

  test("new-stories") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.NewStories()
    testConsoleView.processCommand(dummyInput)
    verify(testController).showNewStories()
  }

  test("top-stories") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.TopStories()
    testConsoleView.processCommand(dummyInput)
    verify(testController).showTopStories()
  }

  test("user") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.User("")
    testConsoleView.processCommand(dummyInput)
    verify(testController).showUserDetail("")
  }

  test("comments") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.Comments(1)
    testConsoleView.processCommand(dummyInput)
    verify(testController).showCommentsById(1,1)
  }

  test("set-page-size") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.SetPageSize(1)
    testConsoleView.processCommand(dummyInput)
    verify(testController).setPageSize(1)
  }

  test("wrongArgs") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.WrongArgs("")
    testConsoleView.processCommand(dummyInput)
    verifyZeroInteractions(testController)
  }

  test("clear-cache") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.ClearCache
    testConsoleView.processCommand(dummyInput)
    verify(testController).clearCache()
  }

  test("help") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.Help
    testConsoleView.processCommand(dummyInput)
    verify(testController).showHelp()
  }

  test("exit") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.Exit
    testConsoleView.processCommand(dummyInput)
    verifyZeroInteractions(testController)
    assert(!testConsoleView.isRunning)
  }

  test("unknown") {
    beforeTest()
    when(testStringParser.parse(dummyInput)) thenReturn Command.Unknown
    testConsoleView.processCommand(dummyInput)
    verifyZeroInteractions(testController)
  }

  def beforeTest(): Unit = {
    reset(testStringParser)
    reset(testController)
  }

}
