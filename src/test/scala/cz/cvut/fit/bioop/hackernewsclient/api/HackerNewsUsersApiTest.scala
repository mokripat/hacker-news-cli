package cz.cvut.fit.bioop.hackernewsclient.api

import cz.cvut.fit.bioop.hackernewsclient.api.user.HackerNewsUsersApiImpl
import org.scalatest.funsuite.AnyFunSuite

class HackerNewsUsersApiTest extends AnyFunSuite {
  val usersApi = new HackerNewsUsersApiImpl()

  test("getStoryJson with working test ID") {
    val workingTestId = "mokripatTest"
    val rawUser = usersApi.getUserJson(workingTestId)
    val correctOutput =
      "{\n  \"created\" : 1634740756,\n  \"id\" : \"mokripatTest\",\n  \"karma\" : 1\n}\n"

    assert(rawUser == correctOutput)
  }

  test("getStoryJson with empty string should throw illegal argument exp") {
    assertThrows[IllegalArgumentException](usersApi.getUserJson(""))
  }

  test("getStoryJson with very exotic string") {
    val exoticString = "/~^&*{}`+/"
    val rawUser = usersApi.getUserJson(exoticString)

    assert(rawUser == "null\n")
  }
}
