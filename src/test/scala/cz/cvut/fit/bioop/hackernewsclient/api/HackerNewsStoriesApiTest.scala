package cz.cvut.fit.bioop.hackernewsclient.api

import cz.cvut.fit.bioop.hackernewsclient.api.item.{HackerNewsItemApi, HackerNewsStoriesApiImpl}
import org.scalatest.funsuite.AnyFunSuite

class HackerNewsStoriesApiTest extends AnyFunSuite {
  val itemsApi: HackerNewsItemApi = new HackerNewsItemApi
  val storiesApi = new HackerNewsStoriesApiImpl(itemsApi)

  test("getStoryJson with working test ID") {
    val workingTestId = 28812520
    val rawStory = storiesApi.getStory(workingTestId)
    val correctOutput =
      "{\n  \"by\" : \"Panino\",\n  \"descendants\" : 0,\n  \"id\" : 28812520,\n  \"score\" : 3,\n  \"time\" : 1633807752,\n  \"title\" : \"A New Global Study Refines Estimates of Rooftop Solar Potential\",\n  \"type\" : \"story\",\n  \"url\" : \"https://news.climate.columbia.edu/2021/10/08/a-new-global-study-refines-estimates-of-rooftop-solar-potential/\"\n}\n"

    assert(rawStory == correctOutput)
  }

  test("getStoryJson with negative input") {
    val rawStory = storiesApi.getStory(-1)
    val emptyJson = "null\n"

    assert(rawStory == emptyJson)
  }

  test("getStoryJson with non-existent ID") {
    val rawStory = storiesApi.getStory(Long.MaxValue)
    val emptyJson = "null\n"

    assert(rawStory == emptyJson)
  }

  test("getTopStoriesIdsBox") {
    assert(storiesApi.getTopStoriesIdsBox.nonEmpty)
  }
  test("getNewStoriesIdsBox") {
    assert(storiesApi.getNewStoriesIdsBox.nonEmpty)
  }
}
