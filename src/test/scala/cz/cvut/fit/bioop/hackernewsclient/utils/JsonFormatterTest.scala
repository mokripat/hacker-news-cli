package cz.cvut.fit.bioop.hackernewsclient.utils

import cz.cvut.fit.bioop.hackernewsclient.model.item.{Comment, Story}
import cz.cvut.fit.bioop.hackernewsclient.model.user.HackerNewsUser
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.FormatExtension.HtlmUnescape
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter
import org.scalatest.funsuite.AnyFunSuite

class JsonFormatterTest extends AnyFunSuite {
  val jsonFormatter = new JsonFormatter()

  test("jsonBoxToList correct input") {
    val longList = "[1,2,3,4,5]"
    val correctOutput = List(1, 2, 3, 4, 5)

    assert(jsonFormatter.jsonBoxToList(longList).get == correctOutput)
  }

  test("jsonBoxToList empty list input") {
    val longList = "[]"
    val correctOutput = List[Long]()

    assert(jsonFormatter.jsonBoxToList(longList).get == correctOutput)
  }

  test("jsonBoxToList incorrect input") {
    val longList = "[1,a,3,4,5]"

    assert(jsonFormatter.jsonBoxToList(longList).isEmpty)
  }

  test("jsonBoxToList empty string input") {
    val longList = ""

    assert(jsonFormatter.jsonBoxToList(longList).isEmpty)
  }

  test("itemJsonToStory with all parameters") {
    val hackerStoryJson =
      "{\"by\":\"ethanyu94\",\"descendants\":2,\"id\":28842116,\"kids\":[123,333],\"score\":1,\"time\":1634058053,\"title\":\"Motion (YC W20) Is Hiring Software Engineers\",\"type\":\"story\",\"url\":\"https://www.workatastartup.com/jobs/47069\"}"
    val correctOutput = Story(
      "ethanyu94",
      28842116,
      1634058053,
      1,
      "Motion (YC W20) Is Hiring Software Engineers",
      "https://www.workatastartup.com/jobs/47069",
      "story",
      2,
      List[Long](123, 333)
    )

    assert(jsonFormatter.itemJsonToStory(hackerStoryJson).get == correctOutput)
  }

  test("itemJsonToStory without optional parameters") {
    val hackerStoryJson =
      "{\"by\":\"ethanyu94\",\"id\":28842116,\"score\":1,\"time\":1634058053,\"title\":\"Motion (YC W20) Is Hiring Software Engineers\",\"type\":\"story\",\"url\":\"https://www.workatastartup.com/jobs/47069\"}"
    val correctOutput = Story(
      "ethanyu94",
      28842116,
      1634058053,
      1,
      "Motion (YC W20) Is Hiring Software Engineers",
      "https://www.workatastartup.com/jobs/47069",
      "story")

    assert(jsonFormatter.itemJsonToStory(hackerStoryJson).get == correctOutput)
  }

  test("userJsonToUser correct input") {
    val userJson =
      "{\"created\":1634740756,\"id\":\"mokripatTest\",\"karma\":1,\"about\":\"Happy testing acc\"}"
    val correctOutput =
      HackerNewsUser("mokripatTest", 1634740756, 1, "Happy testing acc", List())

    assert(jsonFormatter.userJsonToUser(userJson).get == correctOutput)
  }

  test("userJsonToUser json without mandatory arguments should return empty option object") {
    val userJson =
      "{\"created\":1634740756,\"id\":\"mokripatTest\",\"about\":\"Happy testing acc\"}"

    assert(jsonFormatter.userJsonToUser(userJson) == Option.empty)
  }

  test("userJsonToUser empty json input should return empty option object") {
    val emptyUserJson = "{}"
    assert(jsonFormatter.userJsonToUser(emptyUserJson) == Option.empty)
  }

  test("userJsonToUser invalid json input should throw IncompleteParseException") {
    val emptyUserJson1 = "{about:invalidInput}"
    assert(jsonFormatter.userJsonToUser(emptyUserJson1).isEmpty)
  }

  test("userJsonToUser empty input should throw IncompleteParseException") {
    val emptyUserJson2 = ""
    assert(jsonFormatter.userJsonToUser(emptyUserJson2).isEmpty)
  }

  test("itemJsonToComment with all parameters") {
    val commentJson =
      "{\n  \"by\" : \"junon\",\n  \"id\" : 29530181,\n  \"kids\" : [ 29530216 ],\n  \"parent\" : 29529791,\n  \"text\" : \"A good deal of projects over the years have relied on &#x27;re&#x27; at their core. Thank you, Fredrik Lundh.\",\n  \"time\" : 1639321709,\n  \"type\" : \"comment\"\n}"

    val correctOutput = Comment(
      29530181,
      "junon",
      1639321709,
      29529791,
      "A good deal of projects over the years have relied on &#x27;re&#x27; at their core. Thank you, Fredrik Lundh.".unescape(),
      "comment",
      List[Long](29530216)
    )

    assert(jsonFormatter.itemJsonToComment(commentJson).get == correctOutput)
  }

  test("itemJsonToComment without optional parameters") {
    val commentJson =
      "{\n  \"by\" : \"junon\",\n  \"id\" : 29530181,\n  \"parent\" : 29529791,\n  \"text\" : \"A good deal of projects over the years have relied on &#x27;re&#x27; at their core. Thank you, Fredrik Lundh.\",\n  \"time\" : 1639321709,\n  \"type\" : \"comment\"\n}"

    val correctOutput = Comment(
      29530181,
      "junon",
      1639321709,
      29529791,
      "A good deal of projects over the years have relied on &#x27;re&#x27; at their core. Thank you, Fredrik Lundh.".unescape(),
      "comment",
      List[Long]()
    )

    assert(jsonFormatter.itemJsonToComment(commentJson).get == correctOutput)
  }

  test("itemJsonToComment json without mandatory arguments should return empty option object") {
    val commentJson =
      "{\n  \"by\" : \"junon\",\n  \"parent\" : 29529791,\n  \"text\" : \"A good deal of projects over the years have relied on &#x27;re&#x27; at their core. Thank you, Fredrik Lundh.\",\n  \"time\" : 1639321709,\n  \"type\" : \"comment\"\n}"

    assert(jsonFormatter.itemJsonToComment(commentJson) == Option.empty)
  }

  test("itemJsonToComment empty json input should return empty option object") {
    val commentJson = "{}"
    assert(jsonFormatter.itemJsonToComment(commentJson) == Option.empty)
  }
}
