package cz.cvut.fit.bioop.hackernewsclient.database

import org.scalatest.funsuite.AnyFunSuite

class InMemoryDatabaseTest extends AnyFunSuite {
  var testDatabase : InMemoryDatabase[Int,String] = new InMemoryDatabase

  private val itemId = 1
  private val itemData = "Hello"

  test("start empty state") {
    testDatabase.clear()
    assert(testDatabase.size == 0)
  }

  test("add and get item") {
    testDatabase.clear()
    assert(testDatabase.save(itemId, itemData))
    assert(testDatabase.get(itemId) == Option(itemData))
  }

  test("adding existing item") {
    testDatabase.clear()
    assert(testDatabase.save(itemId,itemData))
    assert(!testDatabase.save(itemId,itemData))
    assert(testDatabase.size == 1)
  }

  test("size test after adding single item multiple times") {
    testDatabase.clear()
    val itemsId = List(1,2,3)
    val itemsData = List("Hello ", "World", "!")

    testDatabase.save(itemsId.head,itemsData.head)
    testDatabase.save(itemsId(1),itemsData(1))
    testDatabase.save(itemsId(2),itemsData(2))
    testDatabase.save(itemsId.head,itemsData.head)

    assert(testDatabase.size == 3)
  }

  test("updating item") {
    testDatabase.clear()
    val itemUpdatedData = "hello"

    testDatabase.save(itemId, itemData)
    assert(testDatabase.update(itemId,itemUpdatedData))
    assert(testDatabase.get(itemId).get == itemUpdatedData)
  }

  test("updating non existent item") {
    testDatabase.clear()
    assert(!testDatabase.update(1,"what"))
  }

  test("deleting item") {
    testDatabase.clear()

    testDatabase.save(itemId, itemData)
    assert(testDatabase.remove(itemId))
    assert(testDatabase.get(itemId).isEmpty)
  }

  test("deleting non existent item") {
    testDatabase.clear()
    assert(!testDatabase.remove(1))
  }
}
