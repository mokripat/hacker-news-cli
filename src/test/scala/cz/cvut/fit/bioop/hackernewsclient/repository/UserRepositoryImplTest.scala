package cz.cvut.fit.bioop.hackernewsclient.repository

import cz.cvut.fit.bioop.hackernewsclient.api.user.HackerNewsUsersApi
import cz.cvut.fit.bioop.hackernewsclient.model.user.HackerNewsUser
import cz.cvut.fit.bioop.hackernewsclient.utils.exception.{ApiRequestFailed, InvalidUserIdException}
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter
import org.mockito.MockitoSugar.{mock, when}
import org.scalatest.funsuite.AnyFunSuite

import java.io.IOException

class UserRepositoryImplTest extends AnyFunSuite {
  val testUserId = "mokripatTest"
  val testUser: HackerNewsUser = HackerNewsUser("mokripatTest",1634740756,1,"",List())
  val testRawUserData = "{\n  \"created\" : 1634740756,\n  \"id\" : \"mokripatTest\",\n  \"karma\" : 1\n}\n"

  val usersApi : HackerNewsUsersApi = mock[HackerNewsUsersApi]
  val jsonFormatter : JsonFormatter = mock[JsonFormatter]

  test("getUser happy scenario") {
    when(usersApi.getUserJson(testUserId)) thenReturn testRawUserData
    when(jsonFormatter.userJsonToUser(testRawUserData)) thenReturn Option(testUser)

    val repository = new UserRepositoryImpl(usersApi,jsonFormatter)
    assert(repository.getUser(testUserId) == testUser)
  }

  test("getUser user could not be parsed") {
    when(usersApi.getUserJson(testUserId)) thenReturn testRawUserData
    when(jsonFormatter.userJsonToUser(testRawUserData)) thenReturn Option.empty

    val repository = new UserRepositoryImpl(usersApi,jsonFormatter)
    assertThrows[InvalidUserIdException](repository.getUser(testUserId))
  }

  test("getUser api returned empty json ") {
    when(usersApi.getUserJson(testUserId)) thenReturn "null\n"
    when(jsonFormatter.userJsonToUser("null\n")) thenReturn Option.empty

    val repository = new UserRepositoryImpl(usersApi,jsonFormatter)
    assertThrows[InvalidUserIdException](repository.getUser(testUserId))
  }

  test("getUser api failed") {
    when(usersApi.getUserJson(testUserId)) thenThrow new IOException

    val repository = new UserRepositoryImpl(usersApi,jsonFormatter)
    assertThrows[ApiRequestFailed](repository.getUser(testUserId))
  }

}
