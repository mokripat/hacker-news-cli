package cz.cvut.fit.bioop.hackernewsclient.controller

import cz.cvut.fit.bioop.hackernewsclient.controller.ConsoleController.{API_REQUEST_FAIL_TEXT, CLEAR_CACHE_TEXT, HELP_TEXT, cantFindUserMessage, couldNotFetchStoryMessage, noCommentsMessage, setPageSizeMessage}
import cz.cvut.fit.bioop.hackernewsclient.model.item.{Comment, Story}
import cz.cvut.fit.bioop.hackernewsclient.model.user.HackerNewsUser
import cz.cvut.fit.bioop.hackernewsclient.repository.{CommentsRepository, StoriesRepository, UserRepository}
import cz.cvut.fit.bioop.hackernewsclient.utils.Constants
import cz.cvut.fit.bioop.hackernewsclient.utils.exception.{ApiRequestFailed, InvalidUserIdException}
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.FormatExtension.{CommentsListFormatter, StoriesListFormatter, UserFormatter}
import org.mockito.MockitoSugar.{mock, reset, verify, when}
import org.scalatest.funsuite.AnyFunSuite

class ConsoleControllerTest extends AnyFunSuite {
  val storiesRepository: StoriesRepository = mock[StoriesRepository]
  val userRepository: UserRepository = mock[UserRepository]
  val commentsRepository: CommentsRepository = mock[CommentsRepository]
  val testConsoleController = new ConsoleController(storiesRepository,commentsRepository,userRepository)

  private val testStory1 = Story("admin",1,Long.MaxValue,256,"first story","admin.root","story",2,List(4,5))
  private val testStory2 = Story("pepa",2,Long.MaxValue,256,"second story","admin.root","story",1,List(16))

  test("help") {
    beforeTest()
    assert(testConsoleController.showHelp() == HELP_TEXT)
  }

  test("show top stories") {
    beforeTest()
    val testListOfTopStories = List(
      testStory1,testStory2
    )
    when(storiesRepository.getTopStoriesPage(0)) thenReturn testListOfTopStories
    assert(testConsoleController.showTopStories() == testListOfTopStories.formatIndexed(1))
  }

  test("show new stories") {
    beforeTest()
    val testListOfTopStories = List(
      testStory1,testStory2
    )
    when(storiesRepository.getNewStoriesPage(0)) thenReturn testListOfTopStories
    assert(testConsoleController.showNewStories() == testListOfTopStories.formatIndexed(1))
  }

  test("show comments by id -> nonEmpty") {
    beforeTest()
    val testComment1 = Comment(4,"commentPerson",64,1,"haha comment","comment")
    val testComment2 = Comment(5,"adolf",64,1,"can it work?","comment")
    val testCommentsList = List(testComment1,testComment2)
    when(storiesRepository.getStory(testStory1.itemId)) thenReturn Some(testStory1)
    when(commentsRepository.getStoryCommentsPage(testStory1,0)) thenReturn testCommentsList
    assert(testConsoleController.showCommentsById(testStory1.itemId,1) == testCommentsList.format())
  }


  test("show comments by id -> no comments") {
    beforeTest()
    val storyWithoutComments = testStory1.copy(kids = List())
    when(storiesRepository.getStory(testStory1.itemId)) thenReturn Some(storyWithoutComments)
    when(commentsRepository.getStoryCommentsPage(storyWithoutComments,0)) thenReturn List()
    assert(testConsoleController.showCommentsById(testStory1.itemId,1) == noCommentsMessage(testStory1.itemId,1))
  }

  test("show comments by id -> story could not be fetched") {
    beforeTest()
    val notWorkingStoryId = 42
    when(storiesRepository.getStory(notWorkingStoryId)) thenReturn None

    assert(testConsoleController.showCommentsById(notWorkingStoryId,1) == couldNotFetchStoryMessage(notWorkingStoryId))
  }

  test("clear cache") {
    beforeTest()
    assert(testConsoleController.clearCache() == CLEAR_CACHE_TEXT)
    verify(storiesRepository).clearCache()
  }

  test("set page size") {
    beforeTest()
    assert(testConsoleController.setPageSize(100) == setPageSizeMessage(100))
    assert(testConsoleController.pageSize == 100)
  }

  test("show user detail") {
    beforeTest()
    val testUser = HackerNewsUser("me",1000,1000,"1000",List())
    when(userRepository.getUser("me")) thenReturn testUser
    assert(testConsoleController.showUserDetail("me") == testUser.format())
  }

  test("show user api request failed") {
    beforeTest()
    when(userRepository.getUser("random")) thenThrow new ApiRequestFailed
    assert(testConsoleController.showUserDetail("random") == API_REQUEST_FAIL_TEXT)
  }

  test("show user invalid user id") {
    beforeTest()
    when(userRepository.getUser("wrong")) thenThrow new InvalidUserIdException
    assert(testConsoleController.showUserDetail("wrong") == cantFindUserMessage("wrong"))
  }

  private def beforeTest(): Unit = {
    reset(storiesRepository)
    reset(userRepository)
    testConsoleController.setPageSize(Constants.DEFAULT_PAGE_SIZE)
  }

}
