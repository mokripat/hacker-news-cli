package cz.cvut.fit.bioop.hackernewsclient.utils

import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.FormatExtension.HtlmUnescape
import org.scalatest.funsuite.AnyFunSuite
import scala.io.AnsiColor._

class HtmlUnescapeTest extends AnyFunSuite {

  //paragraph unescape
  test("single occurrence") {
    val htmlText = "<p>Hello</p>"
    assert(htmlText.unescapeParagraphs() == "Hello\n")
  }

  test("multiple occurrences") {
    val htmlText = "<p>Hello</p><p>World !</p>"
    assert(htmlText.unescapeParagraphs() == "Hello\nWorld !\n")
  }

  test("no occurrences") {
    val htmlText = "< p>Hello World !< /p>"
    assert(htmlText.unescapeParagraphs() == "< p>Hello World !< /p>")
  }

  //reserved characters unescape
  test("text containing all reserved characters") {
    val htmlText = "&quot;&amp; is logo of &lt;&apos;Etnetera&apos;&gt;&quot;"
    assert(htmlText.unescapeReservedCharacters() == "\"& is logo of <'Etnetera'>\"")
  }

  test("text containing all reserved characters with some mistakes") {
    val htmlText = "&quot&amp; is logo of &lt;&aposEtnetera&apos&gt;&quot"
    assert(htmlText.unescapeReservedCharacters() == "&quot& is logo of <&aposEtnetera&apos>&quot")
  }

  //hex + href links
  test("converting html link href") {
    val htmlText = "A tangentially related trick involves gamma correction<a href=\"http:&#x2F;&#x2F;www.ericbrasseur.org&#x2F;gamma.html\" rel=\"nofollow\">http:&#x2F;&#x2F;www.ericbrasseur.org&#x2F;gamma.html</a><a href=\"https:&#x2F;&#x2F;superuser.com&#x2F;q&#x2F;579216\" rel=\"nofollow\">https:&#x2F;&#x2F;superuser.com&#x2F;q&#x2F;579216</a>"
    val withoutHexChars = "A tangentially related trick involves gamma correction<a href=\"http://www.ericbrasseur.org/gamma.html\" rel=\"nofollow\">http://www.ericbrasseur.org/gamma.html</a><a href=\"https://superuser.com/q/579216\" rel=\"nofollow\">https://superuser.com/q/579216</a>"
    val withoutHrefTag = "A tangentially related trick involves gamma correctionhttp://www.ericbrasseur.org/gamma.htmlhttps://superuser.com/q/579216"

    val hexCharUnescape = htmlText.unescapeHexCharacters()
    val hrefTagUnescape = hexCharUnescape.unescapeHrefLinks()

    assert(hexCharUnescape == withoutHexChars)
    assert(hrefTagUnescape == withoutHrefTag)
  }

  test("italics") {
    val htmlText = "<i>Italics man</i> <em>Italics as well</em>"
    val correctOutput = s"${BLACK}Italics man$RESET ${BLACK}Italics as well$RESET"

    assert(htmlText.unescapeItalics() == correctOutput)
  }

  test("bolds") {
    val htmlText = "<b>Bolds man</b> <strong>Bolds as well</strong>"
    val correctOutput = s"${BOLD}Bolds man$RESET ${BOLD}Bolds as well$RESET"

    assert(htmlText.unescapeBold() == correctOutput)
  }

}
