package cz.cvut.fit.bioop.hackernewsclient.api

import cz.cvut.fit.bioop.hackernewsclient.api.item.{HackerNewsCommentsApiImpl, HackerNewsItemApi}
import org.mockito.MockitoSugar.{mock, when}
import org.scalatest.funsuite.AnyFunSuite

class HackerNewsCommentsApiImplTest extends AnyFunSuite {
  val itemApi: HackerNewsItemApi = mock[HackerNewsItemApi]
  val testCommentApi = new HackerNewsCommentsApiImpl(itemApi)

  test("get comments") {
    val commentsIdsList = List(4L,5L)
    val rawComment1 = "rawComment1"
    val rawComment2 = "rawComment2"
    when(itemApi.getItem(4)) thenReturn rawComment1
    when(itemApi.getItem(5)) thenReturn rawComment2

    assert(testCommentApi.getComments(commentsIdsList) == List(rawComment1,rawComment2))
  }
}
