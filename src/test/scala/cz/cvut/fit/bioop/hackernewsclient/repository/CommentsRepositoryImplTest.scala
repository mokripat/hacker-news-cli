package cz.cvut.fit.bioop.hackernewsclient.repository

import cz.cvut.fit.bioop.hackernewsclient.api.item.HackerNewsCommentsApi
import cz.cvut.fit.bioop.hackernewsclient.database.proxy.DatabaseCacheProxy
import cz.cvut.fit.bioop.hackernewsclient.model.item.{Comment, Story}
import cz.cvut.fit.bioop.hackernewsclient.utils.formatter.JsonFormatter
import org.mockito.MockitoSugar.{mock, reset, verify, when}
import org.scalatest.funsuite.AnyFunSuite

class CommentsRepositoryImplTest extends AnyFunSuite{
  val commentsApi: HackerNewsCommentsApi = new TestApi()
  val databaseProxy: DatabaseCacheProxy[Long,Comment] = mock[DatabaseCacheProxy[Long,Comment]]
  val jsonFormatter: JsonFormatter = mock[JsonFormatter]

  val testCommentsRepository: CommentsRepositoryImpl = new CommentsRepositoryImpl(commentsApi,databaseProxy,jsonFormatter)

  private val testStory1 = Story("admin",1,Long.MaxValue,256,"first story","admin.root","story",2,List(4,5))

  val rawComment1 = "rawComment1"
  val rawComment2 = "rawComment2"
  val testComment1: Comment = Comment(4,"commentPerson",64,1,"haha comment","comment")
  val testComment2: Comment = Comment(5,"adolf",64,1,"can it work?","comment")

  test("get story comments (saved in cache)") {
    beforeTest()
    when(databaseProxy.get(testComment1.itemId)) thenReturn Some(testComment1)
    when(databaseProxy.get(testComment2.itemId)) thenReturn Some(testComment2)

    assert(testCommentsRepository.getStoryCommentsPage(testStory1,0) == List(testComment1,testComment2))
  }

  test("get story comments (from api)") {
    beforeTest()
    when(databaseProxy.get(testComment1.itemId)) thenReturn None
    when(databaseProxy.get(testComment2.itemId)) thenReturn None

    assert(testCommentsRepository.getStoryCommentsPage(testStory1,0) == List(testComment1,testComment2))
    verify(databaseProxy).save(testComment1.itemId,testComment1)
    verify(databaseProxy).save(testComment2.itemId,testComment2)
  }

  test("get story comments (some from cache, some from api)") {
    beforeTest()
    when(databaseProxy.get(testComment1.itemId)) thenReturn None
    when(databaseProxy.get(testComment2.itemId)) thenReturn Some(testComment2)

    assert(testCommentsRepository.getStoryCommentsPage(testStory1,0) == List(testComment1,testComment2))
    verify(databaseProxy).save(testComment1.itemId,testComment1)
  }


  test("get story comments page 2") {
    beforeTest()
    when(databaseProxy.get(testComment2.itemId)) thenReturn Some(testComment2)

    assert(testCommentsRepository.getStoryCommentsPage(testStory1,1,1) == List(testComment2))
  }


  test("get story comments with page out of range") {
    beforeTest()

    assert(testCommentsRepository.getStoryCommentsPage(testStory1,1,50) == List())
  }

  test("get story comments with page on the edge") {
    beforeTest()
    val testStory = testStory1.copy(kids = List(1,4,5))
    when(databaseProxy.get(testComment2.itemId)) thenReturn Some(testComment2)

    assert(testCommentsRepository.getStoryCommentsPage(testStory,1,2) == List(testComment2))
  }

  test("story without any comments") {
    beforeTest()
    val testStoryWithoutComments = testStory1.copy(kids = List())

    assert(testCommentsRepository.getStoryCommentsPage(testStoryWithoutComments,0) == List())
  }

  test("clear cache") {
    beforeTest()
    testCommentsRepository.clearCache()
    verify(databaseProxy).clear()
  }

  private def beforeTest(): Unit = {
    reset(databaseProxy)
    reset(jsonFormatter)

    when(jsonFormatter.itemJsonToComment(rawComment1)) thenReturn Option(testComment1)
    when(jsonFormatter.itemJsonToComment(rawComment2)) thenReturn Option(testComment2)

    when(databaseProxy.save(testComment1.itemId, testComment1)) thenReturn true
    when(databaseProxy.save(testComment2.itemId, testComment2)) thenReturn true
  }


  class TestApi() extends HackerNewsCommentsApi {
    /**
     * Fetches item comments with given IDs.
     *
     * @param kids IDs of item kids.
     * @return list of kids raw resource.
     */
    override def getComments(kids: List[Long]): List[String] =
      List(rawComment1,rawComment2)

    /**
     * Fetches comment by given ID.
     *
     * @param commentId ID of wanted comment
     * @return raw resource of comment.
     */
    override def getComment(commentId: Long): String =
      commentId match {
        case testComment1.itemId => rawComment1
        case testComment2.itemId => rawComment2
        case _ => ""
      }
  }
}
