package cz.cvut.fit.bioop.hackernewsclient.utils


import cz.cvut.fit.bioop.hackernewsclient.model.command.Command
import cz.cvut.fit.bioop.hackernewsclient.utils.parser.command.CommandParser
import cz.cvut.fit.bioop.hackernewsclient.utils.parser.command.CommandParser.{COMMENTS_WRONG_ARGS, NEW_STORIES_WRONG_ARGS, SET_PAGE_SIZE_WRONG_ARGS, TOP_STORIES_WRONG_ARGS, USER_INCOMPLETE}
import org.scalatest.funsuite.AnyFunSuite

import scala.util.Random

class CommandParserTest extends AnyFunSuite {
  val commandParser = new CommandParser()

  test("wrong input should return unknown command") {
    val number = "5"
    val specialCharacter = "^&*^~"
    val randomWord = Random.nextString(10)

    assert(commandParser.parse(number) == Command.Unknown)
    assert(commandParser.parse(specialCharacter) == Command.Unknown)
    assert(commandParser.parse(randomWord) == Command.Unknown)
  }

  test("stop or exit should return exit command") {
    val exit = "exit"
    val stop = "stop"

    assert(commandParser.parse(exit) == Command.Exit)
    assert(commandParser.parse(stop) == Command.Exit)
  }

  test("extra input after stop or exit does not matter") {
    val exit = "exit -1"
    val stop = "stop top-stories"

    assert(commandParser.parse(exit) == Command.Exit)
    assert(commandParser.parse(stop) == Command.Exit)
  }

  test("user [id] should return user command with id parameter") {
    val user = "user "
    val id = "mokripatTest"
    val command = user.appended(id).mkString

    assert(commandParser.parse(command) == Command.User(id))
  }

  test("user without [id] should return incomplete command") {
    val command = "user"

    assert(commandParser.parse(command) == Command.WrongArgs(USER_INCOMPLETE))
  }

  test(
    "top-stories should return top-stories command with default page size value") {
    val command = "top-stories"

    assert(commandParser.parse(command) == Command.TopStories())
  }

  test("top-stories with specified page index") {
    val command = "top-stories -page 2"

    assert(commandParser.parse(command) == Command.TopStories(2))
  }

  test("top-stories wrong arguments values") {
    val correctPrefix = "top-stories -page "
    val wrongArgs = List(
      "0",
      "-5",
      Random.nextString(5),
      "exit"
    )

    wrongArgs.foreach( arg =>
        assert(
          commandParser.parse(correctPrefix.appended(arg).mkString) == Command.WrongArgs(TOP_STORIES_WRONG_ARGS)
      ))
  }

  test("new-stories correct input") {
    val command = "new-stories -page 2"

    assert(commandParser.parse(command) == Command.NewStories(2))
  }

  test("comments correct input without page arg") {
    val command = "comments 1234"

    assert(commandParser.parse(command) == Command.Comments(1234))
  }

  test("comments correct input with page arg") {
    val command = "comments 1234 -page 2"

    assert(commandParser.parse(command) == Command.Comments(1234, 2))
  }

  test("set-page-size correct input") {
    val command = "set-page-size 5"

    assert(commandParser.parse(command) == Command.SetPageSize(5))
  }

  test("set-page-size wrong input") {
    val command = "set-page-size -1"
    val command1 = "set-page-size 0"
    val command2 = "set-page-size 10000"
    val command3 = "set-page-size " + Random.nextString(4)

    assert(commandParser.parse(command) == Command.WrongArgs(SET_PAGE_SIZE_WRONG_ARGS))
    assert(commandParser.parse(command1) == Command.WrongArgs(SET_PAGE_SIZE_WRONG_ARGS))
    assert(commandParser.parse(command2) == Command.WrongArgs(SET_PAGE_SIZE_WRONG_ARGS))
    assert(commandParser.parse(command3) == Command.WrongArgs(SET_PAGE_SIZE_WRONG_ARGS))
  }

  test("new-stories without args") {
    val command = "new-stories"
    assert(commandParser.parse(command) == Command.NewStories())
  }

  test("wrong arguments handle") {
    val command1 = "new-stories -" + Random.nextString(5)
    val command2 = "top-stories -" + Random.nextString(5)
    val command3 = "comments 2 -" + Random.nextString(5)

    assert(commandParser.parse(command1) == Command.WrongArgs(NEW_STORIES_WRONG_ARGS))
    assert(commandParser.parse(command2) == Command.WrongArgs(TOP_STORIES_WRONG_ARGS))
    assert(commandParser.parse(command3) == Command.WrongArgs(COMMENTS_WRONG_ARGS))
  }

  test("help should return help command") {
    val command = "help"

    assert(commandParser.parse(command) == Command.Help)
  }

  test("clearCache command") {
    val command = "clear-cache"

    assert(commandParser.parse(command) == Command.ClearCache)
  }
}