package cz.cvut.fit.bioop.hackernewsclient.database.proxy

import cz.cvut.fit.bioop.hackernewsclient.database.InMemoryDatabase
import org.mockito.MockitoSugar.{mock, reset, verify, when}
import org.scalatest.funsuite.AnyFunSuite

class DatabaseCacheProxyImplTest extends AnyFunSuite {
  val testDatabase: InMemoryDatabase[Int, String] = mock[InMemoryDatabase[Int, String]]
  val cacheProxy = new DatabaseCacheProxyImpl(testDatabase)

  private val itemId1 = 1
  private val itemData1 = "Hello "
  private val itemData2 = "World"

  def beforeTest(): Unit = {
    reset(testDatabase)
    cacheProxy.clear()
  }

  test("saving and getting item") {
    beforeTest()
    when(testDatabase.save(itemId1,itemData1)) thenReturn true
    when(testDatabase.get(itemId1)) thenReturn Option(itemData1)

    assert(cacheProxy.save(itemId1, itemData1))
    assert(cacheProxy.get(itemId1) == Option(itemData1))
  }

  test("saving and getting item after TTL time") {
    beforeTest()
    cacheProxy.setItemsTTL(2)
    when(testDatabase.save(itemId1,itemData1)) thenReturn true
    when(testDatabase.get(itemId1)) thenReturn Option(itemData1)

    assert(cacheProxy.save(itemId1,itemData1))
    Thread.sleep(2000)
    assert(cacheProxy.get(itemId1).isEmpty)
    verify(testDatabase).remove(itemId1)
  }

  test("getting item that was already in database before using proxy") {
    beforeTest()
    when(testDatabase.get(itemId1)) thenReturn Option(itemData1)

    assert(cacheProxy.get(itemId1).nonEmpty)
  }

  test("trying to save file that is valid in cache -> should fail") {
    beforeTest()
    when(testDatabase.save(itemId1,itemData1)) thenReturn true
    when(testDatabase.get(itemId1)) thenReturn Option(itemData1)

    assert(cacheProxy.save(itemId1,itemData1))
    assert(!cacheProxy.save(itemId1,itemData1))
  }

  test("trying to save file is invalid in cache -> should success") {
    beforeTest()
    cacheProxy.setItemsTTL(2)
    when(testDatabase.save(itemId1,itemData1)) thenReturn true
    when(testDatabase.update(itemId1,itemData1)) thenReturn true
    when(testDatabase.get(itemId1)) thenReturn Option(itemData1)

    assert(cacheProxy.save(itemId1,itemData1))
    Thread.sleep(2000)
    assert(cacheProxy.save(itemId1,itemData1))
  }

  test("trying updating file with success update in database") {
    beforeTest()
    when(testDatabase.update(itemId1,itemData2)) thenReturn true

    assert(cacheProxy.update(itemId1,itemData2))
  }

  test("trying updating file with fail update in database") {
    beforeTest()
    when(testDatabase.update(itemId1,itemData2)) thenReturn false

    assert(!cacheProxy.update(itemId1,itemData2))
  }

  test("clear clears database") {
    beforeTest()
    verify(testDatabase).clear()
  }
}
