# Hacker News Client by @mokripat

This is an implementation of [Hacker News Client](https://courses.fit.cvut.cz/BI-OOP/projects/hackernews-cli.html) as semestral project for BI-OOP done in Scala.

The full project assignment can be found on the official [courses page](https://courses.fit.cvut.cz/BI-OOP/projects/hackernews-cli.html).

## Architecture

This CLI client is using MVC (model-view-controller) architecture:
*  For models there are repositories which handles all manipulation with data (api/database).
*  For view there is consoleView which handles user input, sends command to the controller and prints its response.
*  For controller there is consoleController which methods are called by view, it gets data from repositories, do required stuff with it and sends back response.

## Project Structure

```
.idea/          - Idea specific files
project/        - SBT configuration files
src/
├── main/
│   └── scala/  - Scala source files
└── test/
    └── scala/  - Scala tests
target/
.gitlab-ci.yml  - CI pipeline configuration
.scalafmt.conf  - scalafmt formatting rules
build.sbt       - SBT build file
hnc             - executable script
README.md      
```

## Build

The project can be build either using IntelliJ Idea directly or by using the following `sbt` command:

```
sbt assembly
```

This command compiles the entire code, runs the tests and produces a jar including all dependencies.

## Usage

Same as building, the project can be run through the IntelliJ Idea; by running the sbt command:

```
sbt run
```

After compiling, the application will be run and starts accepting user input (`Listening now.. Enter command:` will be printed)<br><br>
You can view all available commands by typing `help` (if you make any mistake in commands arguments, usage will be shown to guide you)

## Tests

Test can be run by running sbt command
```
sbt test
```

## Sample flows
1)
```
sbt run
new-stories             #show current new stories
new-stories -page 2     #show page 2 of new-stories
user [by]               #find interesting story and want to check user detail who posted with [by] (below the story in [] brackets)
stop                    #to stop the app
```
2)
```
sbt run
set-page-size 20        #sets page sizes to 20
top-stories             #show current top stories
top-stories             #to try caching
clear-cache             #clear cache
top-stories             #fetch main page of top stories again!
exit                    #to stop the app
```
3)
```
sbt run
set-page-size 10        #sets page sizes to 10
top-stories             #show current top stories
comments [id]           #find interesting story with many comments with id ___ (below the story in [] brackets)
comments [id] -page 2   #to check second page of comments
comments [id]           #to check again first page of comments (also test comments caching)
exit                    #to stop the app
```

### Disclaimer
ITALIC IS NOT IN GENERAL ANSI ESCAPE CODES FOR SCALA SO I USED BLACK COLOR INSTEAD https://www.scala-lang.org/api/2.13.3/scala/io/AnsiColor.html<br>
Sometimes page is missing 1-2 stories/comments which is due item is deleted but still included in IDs lists.
